import logging, sys

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

formatter = logging.Formatter(fmt="%(asctime)s %(levelname)s: %(message)s", 
                        datefmt="%Y-%m-%d - %H:%M:%S")
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)

log.addHandler(ch)

def info(Nachricht):
    log.info(Nachricht)

def debug(Nachricht):
    log.debug(Nachricht)
