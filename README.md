# KafkaDemo

## Monolith

![Architektur](MEKONG-Monolith.png)

## Microservice

### Architektur

![Architektur](MEKONG-Komponentenübersicht.png)

### Applikationsübersicht

![Architektur](Microservice.png)
