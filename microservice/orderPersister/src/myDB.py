import mysql.connector
from dynaconf import settings
import myLogger

mydb = mysql.connector.connect(
            host=settings.MYSQL.host,
            user=settings.MYSQL.user,
            password=settings.MYSQL.password,
            database="mysql"
        ) 

mycursor = mydb.cursor()


def test():
    s="dd"
    myLogger.info("Starte DB Verbindung %s"%s)
    


def createDB():

    mycursor.execute("SHOW DATABASES")

    f=[]
    for x in mycursor:
        f.append(x[0])

    myLogger.info("Datenbanken: %s"%f)


    if "orders" in f:
        myLogger.info("DB existiert. Nichts zu machen")
    else:
        myLogger.info("DB existiert nicht. Wird angelegt.")
        mycursor.execute("CREATE DATABASE orders")
        mycursor.execute("""CREATE TABLE orders.orders (
            id INT NOT NULL AUTO_INCREMENT,
            Vorname VARCHAR(45) NULL,
            Nachname VARCHAR(45) NULL,
            Ausweisnummer VARCHAR(45) NULL,
            Start VARCHAR(45) NULL,
            Ziel VARCHAR(45) NULL,
            PRIMARY KEY (id)
        );""")
    
    mycursor.close()
    mydb.close()

def insertOrder(val):
    sql = "insert into orders.orders (Vorname, Nachname, Ausweisnummer, Start, Ziel) VALUES (%s, %s, %s, %s, %s)"    
    mycursor.execute(sql, val)
    mydb.commit()
    mycursor.close()
    mydb.close()
    myLogger.info(str("DB Eintrag geschrieben: %s"%(str(val))))

def fetchAll():
    mycursor.execute("SELECT * FROM orders.orders")
    mycursor.close()
    mydb.close()
    return mycursor.fetchall()

createDB()
