import json
import time

from kafka import KafkaConsumer
from dynaconf import settings

import myLogger
import myDB

consumer = KafkaConsumer(settings.APPCONFIG.ORDERTOPIC,
                            bootstrap_servers=settings.APPCONFIG.KAFKASERVERS,
                            group_id=settings.APPCONFIG.GROUPID,
                            value_deserializer=lambda m: json.loads(m.decode('utf-8')),
                            auto_offset_reset="earliest",
                            consumer_timeout_ms=60000,
                            enable_auto_commit=False)

for message in consumer:
    myDB.insertOrder(message.value)
    consumer.commit()
    myLogger.info(str("Buchung in DB geschrieben: %s"%message.value))

consumer.close()

while True:
    time.sleep(10/1000)
    