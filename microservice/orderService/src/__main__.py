import os, time, random
import json
from kafka import KafkaConsumer
from kafka import KafkaProducer
from dynaconf import settings

import myLogger



# Variablen definieren
###########################################

producer = KafkaProducer(retries=5, bootstrap_servers=settings.APPCONFIG.KAFKASERVERS)

consumer = KafkaConsumer(settings.APPCONFIG.ORDERTOPIC,
                            bootstrap_servers=settings.APPCONFIG.KAFKASERVERS,
                            group_id=settings.APPCONFIG.GROUPID,
                            value_deserializer=lambda m: json.loads(m.decode('utf-8')),
                            auto_offset_reset="earliest",
                            consumer_timeout_ms=60000,
                            enable_auto_commit=False)


# consumer = KafkaConsumer(settings.APPCONFIG.TOPIC,
#                             group_id=settings.APPCONFIG.KAFKAGROUP,
#                             value_deserializer=lambda m: json.loads(
#                                 m.decode('utf-8')),
#                             bootstrap_servers=settings.APPCONFIG.KAFKASERVERS,
#                             auto_offset_reset="earliest")


# Funktionen
###########################################
def fraud_check():
    lower_limit = 0
    upper_limit = 1000
    number = random.randint(lower_limit, upper_limit)
    time.sleep(number  / 1000)
    return random.randint(0,1)

def amadeus_bookFlight():
    lower_limit = settings.APPCONFIG.AMALOWER
    upper_limit = settings.APPCONFIG.AMAUPPER
    number = random.randint(lower_limit, upper_limit)
    time.sleep(number)

def persist_bookedFlight():   
    myLogger.info("Schreibe failed Booking: %s"%json.dumps(message.value).encode('utf-8'))

    producer.send(settings.APPCONFIG.FAILEDBOOKINGTOPIC, json.dumps(message.value).encode('utf-8'))
    producer.flush()
    
    myLogger.info("Schreibe in Kafka confirmedbookings")

def persist_failedBookings():   
    myLogger.info("Schreibe: %s"%json.dumps(message.value).encode('utf-8'))

    producer.send(settings.APPCONFIG.BOOKINGTOPIC, json.dumps(message.value).encode('utf-8'))
    producer.flush()
    
    myLogger.info("Schreibe in Kafka confirmedbookings")

for message in consumer:
    checkok = fraud_check()
    if checkok:
        amadeus_bookFlight()
        persist_bookedFlight()
    else:
        persist_failedBookings()
    message.value.append(checkok)
    myLogger.info(str("Buchung bestätigt: %s"%message.value))
    consumer.commit()

consumer.close()   

while True:
    time.sleep(10/1000)