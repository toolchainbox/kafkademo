from flask import Flask, request
import os, time, random
import json
from kafka import KafkaProducer
from dynaconf import settings

import myLogger



# Variablen definieren
###########################################
app = Flask(__name__)

producer = KafkaProducer(retries=5, bootstrap_servers=settings.APPCONFIG.KAFKASERVERS)

# Funktionen
###########################################
def persistOrder():
    mydata = request.get_json()

    order = (mydata["Vorname"], 
              mydata["Nachname"],
              mydata["Ausweisnummer"],
              mydata["Start"],
              mydata["Ziel"])
    
    myLogger.info("Schreibe: %s"%json.dumps(order).encode('utf-8'))

    producer.send(settings.APPCONFIG.TOPIC, json.dumps(order).encode('utf-8'))
    producer.flush()

    myLogger.info("Schreibe in Kafka orders")



# Routen
###########################################
@app.route('/flights/order', methods=['POST'])
def flightsbooking():
    persistOrder()
    return str("Done %s "%(request.data))


# App starten
###########################################
if __name__ == '__main__':
    port = os.environ.get('FLASK_PORT') or 8080
    port = int(port)
    app.run(port=port,host='0.0.0.0')