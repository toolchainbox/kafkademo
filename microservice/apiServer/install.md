PRJ="apiServer"

mkdir -p ~/dev/kafka/$PRJ/src

cd  ~/dev/kafka/$PRJ/

python3 -m pip install virtualenv
python3 -m virtualenv ~/dev/kafka/$PRJ/
source ~/dev/kafka/$PRJ/bin/activate

#python3 -m pip install dynaconf
#python3 -m pip install flask
#python3 -m pip freeze > src/requirements.txt  

python3 -m pip install -r src/requirements.txt

export DYNACONF_APPCONFIG__FAILRATE=3
export DYNACONF_APPCONFIG__RPS=20

export DYNACONF_MYSQL__host=mariadb
export DYNACONF_MYSQL__user=root
export DYNACONF_MYSQL__password=root