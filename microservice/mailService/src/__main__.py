import json
import time
from kafka import KafkaConsumer
from dynaconf import settings
from email.message import EmailMessage
from smtplib import SMTP, SMTPException
import myLogger

consumer = KafkaConsumer(settings.APPCONFIG.ORDERTOPIC,
                            bootstrap_servers=settings.APPCONFIG.KAFKASERVERS,
                            group_id=settings.APPCONFIG.GROUPID,
                            value_deserializer=lambda m: json.loads(m.decode('utf-8')),
                            auto_offset_reset="earliest",
                            consumer_timeout_ms=60000,
                            enable_auto_commit=False)

for message in consumer:
    msg = EmailMessage()
    msg.set_content(str(message.value))
    msg['Subject'] = settings.SMTP.BETREFF
    msg['From'] = settings.SMTP.ABSENDER
    msg['To'] = settings.SMTP.EMPFAENGER

    try:
        smtpObj = SMTP(settings.SMTP.HOST, port=settings.SMTP.PORT)
        smtpObj.send_message(msg)
        myLogger.info(str("Email an %s versendet"%msg['To']))
        myLogger.info(str("Mail versendet: %s"%message.value))
    except SMTPException as exception:
        myLogger.info(str("Error: unable to send email: %s \n %s",str(msg), str(exception)))
    consumer.commit()

consumer.close()

while True:
    time.sleep(10/1000)
    