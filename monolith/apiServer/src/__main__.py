from flask import Flask, request
import os, time, random
import json

import myLogger
import myDB


# Variablen definieren
###########################################
app = Flask(__name__)


# Funktionen
###########################################
def fraud_check():
    lower_limit = 0
    upper_limit = 1000
    number = random.randint(lower_limit, upper_limit)
    time.sleep(number  / 1000)

def amadeus_bookFlight():
    lower_limit = 7
    upper_limit = 12
    number = random.randint(lower_limit, upper_limit)
    time.sleep(number)

def flight_booking():
    lower_limit = 1
    upper_limit = 1000
    number = random.randint(lower_limit, upper_limit)
    time.sleep(number  / 1000)
    
    mydata = request.get_json()

    val = (mydata["Vorname"], 
              mydata["Nachname"],
              mydata["Ausweisnummer"],
              mydata["Start"],
              mydata["Ziel"])
    myDB.insertOrder(val)


# Routen
###########################################
@app.route('/')
def hello():
    return myDB.fetchAll()

@app.route('/flights/order', methods=['POST'])
def flightsbooking():
    #print(request.data)
    fraud_check()
    amadeus_bookFlight()
    flight_booking()
    return str("Done %s "%(request.data))


# App starten
###########################################
if __name__ == '__main__':
    port = os.environ.get('FLASK_PORT') or 8080
    port = int(port)
    app.run(port=port,host='0.0.0.0')